### ObjectBrowser

Webapp for demonstrating the features of ChocolateCloud ObjectStorage. 

Users can connect their cloud storage accounts and use them as a general-purpose distributed object storage.

This demo includes the ObjectStorage library, cross-compiled to Javascript with Emscripten.