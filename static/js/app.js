'use strict';


function BucketListController($scope, $timeout){

    $scope.hoveredBucket = null;
    var timeoutPromise = null;

    $scope.hoverBucketEnd = function(){
        console.log("hoverBucketEnd");

        if(timeoutPromise !== null){
            $timeout.cancel(timeoutPromise);
        }
        if($scope.hoveredBucket !== null){
            $scope.hoveredBucket.showDetails = false;
        }
        $scope.hoveredBucket = null;
        for(var i=0 ; i<$scope.buckets ; i++){
            delete $scope.buckets.showDetails;
        }
    };
    // call it when the list loads
    $scope.hoverBucketEnd();

    $scope.hoverBucketStart = function(bucket){
        console.log("hoverBucketStart: " + bucket.key);
        for(var i=0 ; i<$scope.buckets ; i++){
            delete $scope.buckets.showDetails;
        }
        $scope.hoveredBucket = bucket;

        timeoutPromise = $timeout(function(){
            console.log("hoverBucketStart timeout reached: " + bucket.key);
            if($scope.hoveredBucket != null){
                console.log("hoverBucketStart still hovering: " + bucket.key);
                // still hovering after a second
                bucket.showDetails = true;
            }
        }, 1000)
    }

}

function BucketDetailsController($scope, $http, $routeParams, $timeout){

    $scope.bucketKey = $routeParams.bucketKey;

    // Metadata and contents of the selected bucket
    $scope.bucketData = {};
    $scope.bucketContents = null;
    $scope.loadBucketContents = function(){
        console.log("loading bucket contents at " + $scope.bucketKey);
        $http.get("static/json/bucket-contents.json").then(
            function(result){
                $scope.bucketData = {
                    defaults: result.data.defaults,
                    locations: result.data.locations
                };
                $scope.bucketContents = result.data.contents;

                console.log("objects", $scope.bucketContents);
            },
            function(error){
                console.error("Error getting contents of bucket '"+bucketName+"'", error);
            }
        )
    };
    // call when loaded
    $scope.loadBucketContents();

    $scope.$watch("bucketContents", function(){
        $scope.showBucketContents = $scope.bucketContents !== null;
    });


    $scope.selectedObjectVersions = null;

    $scope.selectedObjectKey = "";
    $scope.loadObjectDetails = function(o){
        if(o.key == $scope.selectedObjectKey){
            // clicked on the same object which is open, close it now
            $scope.closeDetails();
            return;
        }

        var closeFirst = false;
        if($scope.selectedObjectVersions !== null){
            // There's something else that has to go first
            $scope.closeDetails();
            closeFirst = true
        }
        $timeout(function(){
            $scope.selectedObjectVersions = [];
            $scope.selectedObjectKey = o.key;

            downloadObjectDetails(o.objectMetaTag, addObjectVersion);

        }, closeFirst ? 100 : 0);

    };



    function addObjectVersion(version){

        console.log("Adding object version: " + version.versionCode);
        // marking changes in attributes
        if($scope.selectedObjectVersions.length > 0){

            var laterObject = $scope.selectedObjectVersions[0];
            var previousObject = version;

            for(var key in laterObject.attributes){

                if(previousObject.attributes[key] === undefined){
                    // this key appeared now, highlight key and value
                    laterObject.attributes[getHighlightedString(key, "New key in this version")] = getHighlightedString(laterObject.attributes[key], "New key in this version");
                    delete laterObject.attributes[key];
                }
                else if(previousObject.attributes[key] !== laterObject.attributes[key]){
                    // changed, highlight value
                    laterObject.attributes[key] = getHighlightedString(laterObject.attributes[key], "Changed from '" + previousObject.attributes[key] + "'");
                }
                else{
                    // did not change
                }

            }
        }

        // Add the new version to the beginning (=unshift)
        $scope.selectedObjectVersions.unshift(version);

        if($scope.selectedObjectVersions.length == 1){
            // slide in when the first version is added
            $scope.isNarrow = true;
            $timeout(function(){
                // Force the details section animation to the next digest cycle, so it starts after the main section begins shrinking
                $scope.isNarrowDetails = true;
            },0);
        }

        // Load previous version if there's a tag
        if(version.previousVersionTag !== undefined && version.previousVersionTag !== ""){
            $timeout(function(){
                downloadObjectDetails(version.previousVersionTag, addObjectVersion);
            }, ($scope.selectedObjectVersions.length == 1) ? 300 : 0);
            // wait a bit when the first version is added, so the float-in animation can finish before adding the previous versions

        }
    }

    function getHighlightedString(string, changeStr){
        return '<span class="highlight" title="'+changeStr+'">'+string+'</span>';
    }

    function downloadObjectDetails(metaFileTag, callback){

        $http.get("static/json/" + metaFileTag).then(
            function(result){
                callback(result.data);
            }, function(error){
                console.error(error);
            });
    }

    $scope.versionsExpanded = false;
    $scope.toggleVersions = function(){

        // reverse array to show the most recent version on the top
        //$scope.selectedObjectVersions.reverse();
        $scope.versionsExpanded = !$scope.versionsExpanded;
    }

    $scope.closeDetails = function(){
        $scope.versionsExpanded = false;
        $scope.selectedObjectKey = null;

        $scope.isNarrowDetails = false;
        $timeout(function(){
            $scope.isNarrow = false;
            $scope.selectedObjectVersions = null;
        }, 100)

    }

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        if(evt.target.id === "newFolderName"){
            return true;
        }
        $scope.$apply(function(){
            $scope.globalKeypress(evt.keyCode, evt);
        });
    };

    const KEY_ESC = 27;
    $scope.globalKeypress = function(keyCode){
        switch(keyCode){
            case KEY_ESC:
                $scope.closeDetails();
                break;
            default: break;
        }
    }


    $scope.getLocationLogo = function(locationId){
        var loc = getLocation(locationId);
        if($scope.isAccountConnected(loc)){
            return "static/img/clouds/thumb/" + loc.provider + "_brown.png";
        }
        else{
            return "static/img/clouds/thumb/" + loc.provider + "_grey.png";
        }

    };

    $scope.getLocationHint = function(locationId){
        var loc = getLocation(locationId);
        if($scope.isAccountConnected(loc)){
            return loc.hint;
        }
        else{
            return loc.hint + " (not connected)";
        }

    }

    function getLocation(id){
        for(var i=0 ;i<$scope.bucketData.locations.length ; i++) {
            var loc = $scope.bucketData.locations[i];
            if(loc.id == id){
                return loc;
            }
        }
    }


}

/* App Module */
var app = angular.module(
        'chocolatecloud-objectstorage-browser',
        [
            'chocolatecloud-filters',
            'ngRoute',
            'ngResource',
            'ngSanitize'
        ]
).config(["$routeProvider", function($routeProvider){
    $routeProvider
        .when("/bucket/:bucketKey", {templateUrl: 'static/partials/bucketDetails.tpl.html', controller: BucketDetailsController})
        .otherwise({templateUrl: 'static/partials/bucketsList.tpl.html', controller: BucketListController})
}]);


app.controller("MainController", function($scope, $http, $timeout, $location) {

    console.log("MainController loaded")

    $scope.navigateTo = function(location){
        $location.path(location);
    }

    // $scope.accounts = [
    //     {
    //         "provider": "dropbox",
    //         "userId": "maco13@gmail.com",
    //     },
    //     /*{
    //         "provider": "google_drive",
    //         "userId": "chocolate.cloud.demo+daniel@gmail.com",
    //     },*/
    //     {
    //         "provider": "owncloud",
    //         "userId": "chocolate.cloud.demo+frank@gmail.com",
    //     },
    //     {
    //         "provider": "onedrive",
    //         "userId": "maco13@gmail.com",
    //     },
    //     /*
    //     ,{
    //         "provider": "dropbox",
    //         "userId": "chocolate.cloud.demo+frank@gmail.com",
    //     },
    //      {
    //      "provider": "hidrive",
    //      "userId": "maco13@gmail.com"
    //      }
    //     */
    // ];

    $scope.buckets = [];
    $scope.loading = false;

    $scope.loadBuckets = function(){
        $scope.loading = true;
        //$http.get("static/json/buckets.json").then(
        $http.get("backend/index.php?command=list").then(
            function(data){
                // sort by 'created' so the newest ones come first
                data.data.sort(function(a,b){ return ((a.created > b.created) ? -1 : ((a.created < b.created) ? 1 : 0)) })
                $scope.buckets = data.data;
                $scope.loading = false;
                //loadBucketContents($scope.buckets[0].contentsTag);
            },
            function(error){
                console.log("buckets error:", error);
            }
        );
    };
    // call on start
    $scope.loadBuckets();




    $scope.accounts = [
        {
            "provider": "LocalStorage",
            "userId": "StoragesROOT/Account1/",
            "hint": "Local storage at 'Account1/'"
        },
        {
            "provider": "LocalStorage",
            "userId": "StoragesROOT/Account2/",
            "hint": "Local storage at 'Account2/'"
        },
        {
            "provider": "LocalStorage",
            "userId": "StoragesROOT/Account3/",
            "hint": "Local storage at 'Account3/'"
        },
        {
            "provider": "LocalStorage",
            "userId": "StoragesROOT/Account4/",
            "hint": "Local storage at 'Account4/'"
        },
        {
            "provider": "LocalStorage",
            "userId": "StoragesROOT/Account5/",
            "hint": "Local storage at 'Account5/'"
        },
        {
            "provider": "LocalStorage",
            "userId": "StoragesROOT/Account6/",
            "hint": "Local storage at 'Account6/'"
        }
    ];

    $scope.hoveredAccount = null;
    $scope.hoverAccount = function(acc){
        $scope.hoveredAccount = acc;
    }

    $scope.areSameAccounts = function(localAccount, locationObj){
        return (localAccount.provider === locationObj.provider) && (Sha1.hash(localAccount.userId) === locationObj.userIdHash);
    };

    $scope.isAccountConnected = function(locationObj){
        for(var i=0 ;i<$scope.accounts.length; i++){
            if($scope.areSameAccounts($scope.accounts[i], locationObj)){
                return true;
            }
        }
        return false;
    }


    function overhead(storages, losses){
        var rate = 100/(storages - losses);
        return  Math.round(storages * rate)-100 + "%";
    }


    $scope.range = function(howmany){
        var a = [];
        for(var i=0 ; i<howmany ; i++){
            a.push(i);
        }
        return a;
    }

    $scope.getRandPercent = function(){
        return Math.floor(Math.random() * 100) + 1 + "%";
    }

});
