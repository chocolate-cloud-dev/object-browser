function timestamp() {
    if (performance.now !== undefined) {
        return performance.now();
    }

    return new Date().getTime();
}

/**
 * Super-lightweight http service object
 * 
 * @param {String} method (GET)
 * @param {String} url
 * @param {boolean} async (true)
 * @param {Object} self : Reference to the Connector object that's initiating the call (needed for automatic token refresh)
 * @param {Object} headers ({})
 * @param {boolean} transformData (false)
 * @param {boolean} parseResponse (false)
 * @param {boolean} reportSpeed (false)
 * @param {boolean} isWebDAV (false)
 * @param {String | Object | undefined} data (null)
 * @param {Object | undefined} passObject (null) : an object that is passed to the response callbacks
 * @param {Array | undefined} retryOnCodes : response codes upon which the original request should be retried
 * @param {Function} success
 * @param {Function} error
 */
var $http = function(requestConfig, isRetry) {
    
    if (isRetry === undefined) {
        isRetry = false;
    }
    
    if (requestConfig.url === undefined) {
        console.error("$http error: URL must be set");
        console.error(requestConfig);
        return;
    }
    
    // encode url if not disabled by self.HTTP_CONFIG.disable_encode_urls
    if(requestConfig.self === undefined || requestConfig.self.HTTP_CONFIG === undefined || requestConfig.self.HTTP_CONFIG.disable_encode_urls !== true){
        requestConfig.url = encodeURI(requestConfig.url);
    }
    
    
    
    // for debugging $http 
    //var id = Math.round(Math.random() * 1000);

    var request = new XMLHttpRequest();
    // default values
    requestConfig.retryOnCodes = requestConfig.retryOnCodes === undefined ? [] : requestConfig.retryOnCodes;
    requestConfig.async = (requestConfig.async === undefined) ? true : requestConfig.async;
    requestConfig.isWebDAV = (requestConfig.isWebDAV === undefined) ? false : requestConfig.isWebDAV;
    requestConfig.parseResponse = (requestConfig.parseResponse === undefined) ? false : requestConfig.parseResponse;
    requestConfig.method = requestConfig.method || "GET";
    requestConfig.data = requestConfig.data || null;
    requestConfig.passObject = requestConfig.passObject || null;
    requestConfig.reportSpeed = false;  
    requestConfig.headers = requestConfig.headers || {};
    
    if (!requestConfig.isWebDAV && "transformData" in requestConfig && requestConfig.transformData && requestConfig.data) {
        var dataParts = [];

        for (var key in requestConfig.data) {
            dataParts.push(key + "=" + requestConfig.data[key]);
        }
        requestConfig.data = dataParts.join("&");
        
        // shouldn't be transformed again (on retries)
        delete requestConfig.transformData;

        // add urlencoded header
        requestConfig.headers["Content-Type"] = "application/x-www-form-urlencoded";
    }
    
    if(requestConfig.isWebDAV){
        request.withCredentials = true;
    }
    
    if(requestConfig.onProgress !== undefined){
        request.addEventListener("progress", function(progressEvent){
            requestConfig.onProgress(progressEvent.loaded);
        }, false);
    }
    
    // make a hard copy of the data, in case of retry
    var dataCopy = requestConfig.data;
    if(!isRetry && !requestConfig.isWebDAV){
        if(requestConfig.data !== null && requestConfig.data.BYTES_PER_ELEMENT !== undefined){
            
            dataCopy = new ArrayBuffer(requestConfig.data.byteLength);
            // TODO this cloning strategy might NOT WORK UNDER IE
            new Uint8Array(dataCopy).set(new Uint8Array(requestConfig.data));
        }
    }
    
    request.open(requestConfig.method, requestConfig.url, requestConfig.async);
    
    // Add authorization
    // (this should be done _BEFORE_ setting all headers so the client can override the authorization if needed)
    if(!requestConfig.isWebDAV && requestConfig.self !== undefined && requestConfig.self.accountData.access_token !== undefined){
        request.setRequestHeader("Authorization", "Bearer " + requestConfig.self.accountData.access_token);
    }
    
    // add headers
    for (var headerKey in requestConfig.headers) {
        request.setRequestHeader(headerKey, requestConfig.headers[headerKey]);
    }
    // set response type
    if (requestConfig.responseType !== undefined) {
        request.responseType = requestConfig.responseType;
    }

    var requestSent = 0.0;
    var requestTime = 0.0;
    if (requestConfig.async) {
        
        if(requestConfig.reportSpeed){
            request.upload.onloadend = function(e) {
                requestTime = (timestamp() - requestSent).toFixed(2);
            };
        }
        
        request.onerror = function(_1){
            
            //console.error("HTTP request failed"); console.error(_1);
            if(requestConfig.self.STORAGE_TYPE === "box"){
                //console.log("Box fail, try to refresh token");
                requestConfig.self._refreshToken(function(){
                    // retry original request on successful token refresh
                    requestConfig.data = dataCopy;
                    $http(requestConfig, true);
                    //console.log("Access token refreshed successfully");
                });
                return;
            };
        };

        request.onreadystatechange = function() {
            if (request.readyState !== 4) {
                // skip intermediate steps of request state
                return;
            }
            
            var responseType = request.getResponseHeader("Content-Type");
            if(requestConfig.parseResponse !== false && responseType !== null && (responseType.indexOf("application/json") >= 0 || responseType.indexOf("text/javascript") >= 0)){
                requestConfig.parseResponse = true;
            }

            if (request.status >= 200 && request.status < 300) {
                
                if(requestConfig.reportSpeed){
                    if (requestConfig.method === "GET" || requestTime === 0.0){
                        //download
                        requestTime = (timestamp() - requestSent).toFixed(2);
                    }
                    
                    var payloadSize;
                    if(requestConfig.method === "GET"){
                        payloadSize = request.responseType === "arraybuffer" ? request.response.byteLength : request.responseText.length;
                    }
                    else{
                        // ArrayBuffer size: requestConfig.data.byteLength
                        // Blob size: requestConfig.data.size
                        // Anything else size: requestConfig.data.length
                        payloadSize = (requestConfig.data.byteLength === undefined ? (requestConfig.data.size !== undefined ? requestConfig.data.size : requestConfig.data.length) : requestConfig.data.byteLength);
                    }
                    
                    requestConfig.self.broadcastFunction({
                        event: "speed_report",
                        direction: requestConfig.method === "GET" ? "down" : "up",
                        account: requestConfig.self._getCompactAccount(),
                        speed: ((payloadSize * 8 / 1000) / ((requestTime / 1000) * 1.0)).toFixed(2)
                    });
                }
                
                // call success handler
                if (requestConfig.success) {
                    // there is a success handler defined
                    
                    if (requestConfig.parseResponse !== false){
                        requestConfig.success(JSON.parse(request.responseText), requestConfig.passObject, request.status);
                    }
                    else {
                        requestConfig.success(
                                request.responseType === "arraybuffer" ? request.response : request.responseText,
                                requestConfig.passObject, request.status
                                );
                    }
                }
                
            }
            
            else if (request.status !== 0) {
                // HTTP 0 = request aborted, no-op
                
                // Automatically refresh expired token and re-send the original request
                // The well-educated clouds report expired token by sending the response with HTTP 401 (Unauthorized) status code.
                // Microsoft OneDrive reports by sending a HTTP 400 (Bad request) response, and includes a 'WWW-Authenticate' header with value: 'Bearer realm="OneDriveAPI", error="expired_token", error_description="Auth token expired. Try refreshing."'
                //if(request.status === 401 || (request.getResponseHeader("WWW-Authenticate") !== null && request.getResponseHeader("WWW-Authenticate").indexOf("expired_token") >= 0)){
                if(request.status === 401 || (requestConfig.self !== undefined && requestConfig.self.accountData.type === "onedrive" && request.status === 400)){
                    console.log("Refreshing token...");
                    if(requestConfig.self !== undefined && requestConfig.self._refreshToken !== undefined){
                        requestConfig.self._refreshToken(function(){
                            // retry original request on successful token refresh
                            requestConfig.data = dataCopy;
                            $http(requestConfig, true);
                            console.log("Access token refreshed successfully");
                        });
                        return;
                    }
                }
                
                // try again once on 5xx errors
                else if (requestConfig.retryOnCodes.indexOf(request.status) >= 0 || (request.status > 500 && request.status < 600)) {
                    if(!isRetry){
                        console.error("Retrying request");
                        requestConfig.data = dataCopy;
                        $http(requestConfig, true);
                        return;
                    }
                    else{
                        // second fail, it's a cloud error.
                        // Parse out the domain
                        var urlEnd = "";
                        if(requestConfig.url.indexOf("https://") >= 0){
                            urlEnd = requestConfig.url.replace("https://", "");
                        }
                        else{
                            urlEnd = requestConfig.url.replace("http://", "");
                        }
                        
                        requestConfig.self.broadcastFunction({
                            event: "cloud_provider_error",
                            message: JSON.stringify(request.responseText),
                            domain: urlEnd.substring(0, urlEnd.indexOf("/")),
                            responseCode: request.status
                        });
                    }
                    
                }

                // call error handler
                else if (requestConfig.error !== undefined) {
                    requestConfig.error(
                            (request.responseType === "arraybuffer") ? ("HTTP " + request.status) : request.responseText,
                            request.status,
                            requestConfig.passObject
                            );
                }
                else {
                    console.error("Error during " + requestConfig.method + " call to " + requestConfig.url + ": HTTP " + request.status);
                    if(request.responseType.indexOf("json") >= 0){
                        console.error(JSON.parse(request.responseText));
                    }
                    else{
                        console.error(request.responseText);
                    }
                    
                }
            }
        };
        requestSent = timestamp();
        try {
            request.send(requestConfig.data);
        } catch (ex) {
            //console.error(ex);
            try {
                // try with the underlying ArrayBuffer
                request.send(requestConfig.data.buffer);
            } catch (eex) {
                console.error(eex.toString());
            }
        }

    }
    else {
        
        
        
        
        // sync request
        if(requestConfig.reportSpeed){
            request.upload.onloadend = function(e) {
                requestTime = (timestamp() - requestSent).toFixed(2);
                //console.log(id + " Request time until uploadEnd: " + time + " (request send: "+requestSent+")");
            };
            requestSent = timestamp();
        }
        
        request.onerror = function(){
            
            //console.error("HTTP request failed"); console.error(_1);
            if(requestConfig.self.STORAGE_TYPE === "box"){
                //console.log("Box fail, try to refresh token");
                requestConfig.self._refreshToken(function(){
                    // retry original request on successful token refresh
                    requestConfig.data = dataCopy;
                    $http(requestConfig, true);
                    //console.log("Access token refreshed successfully");
                });
            };
        };
        
        // request.send(requestConfig.data);
        try {
            request.send(requestConfig.data);
        } catch (ex) {
            //console.error(ex);
            try {
                // try with the underlying ArrayBuffer
                request.send(requestConfig.data.buffer);
            } catch (eex) {
                //console.error(eex.toString());
                try{
                    request.send(new Blob([requestConfig.data]));
                }catch(eeex){
                    console.error(eeex.toString());
                }
                
            }
        }
        

        var responseType = request.getResponseHeader("Content-Type");
        if(requestConfig.parseResponse !== false && responseType !== null && (responseType.indexOf("application/json") >= 0 || responseType.indexOf("text/javascript") >= 0)){
            requestConfig.parseResponse = true;
        }

        if (request.status >= 200 && request.status < 300) {
            // measure time if we were downloading
            if(requestConfig.reportSpeed){
                if (requestConfig.method === "GET" || requestTime === 0.0){
                    //download
                    requestTime = (timestamp() - requestSent).toFixed(2);
                }
                var payloadSize;
                if(requestConfig.method === "GET"){
                    payloadSize = request.responseType === "arraybuffer" ? request.response.length : request.responseText.length;
                }
                else{
                    payloadSize = (requestConfig.data.byteLength === undefined ? requestConfig.data.length : requestConfig.data.byteLength);
                }
                
                requestConfig.self.broadcastFunction({
                    event: "speed_report",
                    direction: requestConfig.method === "GET" ? "down" : "up",
                    account: requestConfig.self._getCompactAccount(),
                    speed: ((payloadSize * 8 / 1000) / ((requestTime / 1000) * 1.0)).toFixed(2)
                });
            }


            // 2xx status codes are generally positive, run success handler
            if (requestConfig.success) {
                // there is a success handler defined
                if (requestConfig.parseResponse) {
                    requestConfig.success(JSON.parse(request.responseText), requestConfig.passObject, request.status);
                }
                else {
                    requestConfig.success(request.responseText, requestConfig.passObject, request.status);
                }
            }
        }
        
        else if (request.status !== 0) {
            
            // Automatically refresh expired token and re-send the original request
            //if(request.status === 401 || (request.getResponseHeader("WWW-Authenticate") !== null && request.getResponseHeader("WWW-Authenticate").indexOf("expired_token") >= 0)){
            if(request.status === 401 || (requestConfig.self !== undefined && requestConfig.self.accountData.type === "onedrive" && request.status === 400)){
                if(requestConfig.self !== undefined && requestConfig.self._refreshToken !== undefined){
                    requestConfig.self._refreshToken(function(){
                        // retry original request on successful token refresh
                        requestConfig.data = dataCopy;
                        $http(requestConfig, true);
                    });
                    return;
                }
            }
            
            // try again once on 5xx errors
            else if (!isRetry && (requestConfig.retryOnCodes.indexOf(request.status) >= 0 || (request.status >= 500 && request.status < 600))) {
                console.error("Retrying request");
                requestConfig.data = dataCopy;
                $http(requestConfig, true);
                return;
            }
            // call error handler
            else if (requestConfig.error !== undefined) {
                requestConfig.error(request.responseText, request.status, requestConfig.passObject);
            }
            else{
                console.error("Error during " + requestConfig.method + " call to " + requestConfig.url + ": HTTP " + request.status);
                if(request.responseType.indexOf("json") >= 0){
                    console.error(JSON.parse(request.responseText));
                }
                else{
                    console.error(request.responseText);
                }
            }
        }
    }

    return request;
};


function timestamp(){
    try{
        if(performance!== undefined && performance.now !== undefined){
            return performance.now();
        }
    } catch(ex){
        
    }
    
    return new Date().getTime();
}