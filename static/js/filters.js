'use strict';


/* Filters */
angular.module('chocolatecloud-filters', [])
        .filter('implode', function () {
            return function (input, delimiter) {
                var out = "";
                if (delimiter === "ul") {
                    out += "<ul>";
                }

                for (var idx in input) {
                    if (delimiter === "paragraph") {
                        out += "<p>" + input[idx] + "</p>";
                    }
                    else if (delimiter === "break") {
                        out += input[idx] + "<br/>";
                    }
                    else if (delimiter === "ul") {
                        out += "<li>" + input[idx] + "</li>";
                    }
                    else {
                        out += input[idx] + delimiter;
                    }
                }
                if (delimiter === "ul") {
                    out += "</ul>";
                }

                if (delimiter === "paragraph" || delimiter === "break" || delimiter === "ul") {
                    return out;
                }
                return out.substring(0, out.length - delimiter.length) + "";
            };
        })
        .filter('ellipsize', function () {
            return function (input, characters) {
                if (input === undefined) {
                    return "";
                }
                input = input.trim();
                if (input.length <= characters)
                    return input;
                return input.substr(0, characters) + "...";
            };
        })
        .filter('numberName', function () {
            return function (numeric) {
                switch(numeric){
                    case 0: return "zero";
                    case 1: return "one";
                    case 2: return "two";
                    case 3: return "three";
                    case 4: return "four";
                    case 5: return "five";
                    case 6: return "six";
                    case 7: return "seven";
                    case 8: return "eight";
                    case 9: return "nine";
                    case 10: return "ten";
                    case 11: return "eleven";
                    case 12: return "twelve";
                    default: return numeric;
                }
            };
        })

    .filter('uppercase', function(){
        return function(str){
            return str.toUpperCase();
        }
    })

    .filter('reverseIfNeeded', function(){
        return function(array, isReverseNeeded){
            if(!isReverseNeeded){
                return array;
            }
            else{

                var arrCopy = [];
                array.forEach(function(elem){
                    arrCopy.unshift(elem);
                });
                return arrCopy;
            }
        }
    })

    .filter('dataTable', function () {
        return function (object) {
            var str = "";
            for(var key in object){
                if(key[0] == "$"){
                    continue;
                }
                str += "<b>" + key + ": </b>" + object[key] + " <br/>";
            }
            return str;
        };
    })

        .filter('timestamp', function () {
            return function (unixTime, nullValueString) {
                unixTime = unixTime - 0;
                console.log("Timestamp: ", unixTime);
                if (unixTime === null || unixTime === 0 || unixTime === "") {
                    if (nullValueString === undefined) {
                        nullValueString = "";
                    }
                    return "<i>" + nullValueString + "</i>";
                }

                var date = new Date(unixTime);
                var month = date.getMonth();
                month++;
                if (month < 10)
                    month = "0" + month;
                var day = date.getDate();
                if (day < 10)
                    day = "0" + day;
                var hour = date.getHours();
                if (hour < 10)
                    hour = "0" + hour;
                var minute = date.getMinutes();
                if (minute < 10)
                    minute = "0" + minute;

                return date.getFullYear() + "." + month + "." + day + " " + hour + ":" + minute;
            };
        })
        .filter('file_timestamp', function () {
            return function (unixTime) {
                var date = new Date(unixTime);
                var month = date.getMonth();
                month++;
                if (month < 10)
                    month = "0" + month;
                var day = date.getDate();
                if (day < 10)
                    day = "0" + day;
                var hour = date.getHours();
                if (hour < 10)
                    hour = "0" + hour;
                var minute = date.getMinutes();
                if (minute < 10)
                    minute = "0" + minute;
                var seconds = date.getSeconds();
                if (seconds < 10)
                    seconds = "0" + seconds;

                return date.getFullYear() + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + seconds;
            };
        })



        .filter('removeDeleted', function () {
            return function (array) {
                var ret = [];
                if(array == null){
                    return ret;
                }

                for(var i=0 ;i<array.length ; i++) {
                    if (array[i].deleted === undefined) {
                        ret.push(array[i]);
                    }
                }
                return ret;
            };
        })

        .filter('sanitizeString', function () {
            return function (originalString) {
                var from = ["á", "é", "ö", "ő", "ó", "ű", "ú", "ü", "í", " ", "Á", "É", "Ö", "Ő", "Ó", "Ű", "Ú", "Ü", "Í"];
                var to = ["a", "e", "o", "o", "o", "u", "u", "u", "i", "_", "A", "E", "O", "O", "O", "U", "U", "U", "I"];

                for (var idx in from) {
                    var exp = new RegExp(from[idx], "g");
                    originalString = originalString.replace(exp, to[idx]);
                }
                return originalString;

            };
        })
        .filter('fileSize', function () {
            return function (sizeInBytes, transform) {

                if (sizeInBytes > 1000000000) {
                    if (transform === "round") {
                        return Math.round((sizeInBytes / 1024000000)) + "GB";
                    }
                    return (sizeInBytes / 1000000000).toFixed(2) + "GB";
                }
                if (sizeInBytes > 1000000) {
                    if (transform === "round") {
                        return Math.round((sizeInBytes / 1000000)) + "MB";
                    }
                    return (sizeInBytes / 1000000).toFixed(2) + "MB";
                }
                if (sizeInBytes > 1000) {
                    if (transform === "round") {
                        return Math.round((sizeInBytes / 1000)) + "kB";
                        //return Math.round((sizeInBytes/1024));
                    }
                    return (sizeInBytes / 1000).toFixed(2) + "kB";
                    //return (sizeInBytes/1024).toFixed(2);
                }
                return sizeInBytes + " bytes";
                //return sizeInBytes;
            };
        })

    .filter('elapsed', function(){
        return function(timestamp){
            if (!timestamp) return;
            if(timestamp.toString().length == 10){
                // given in seconds instead of ms, correct
                timestamp *= 1000;
            }
            var time = new Date(timestamp).getTime(),
                timeNow = new Date().getTime(),
                difference = timeNow - time,
                seconds = Math.floor(difference / 1000),
                minutes = Math.floor(seconds / 60),
                hours = Math.floor(minutes / 60),
                days = Math.floor(hours / 24);
            if(isNaN(difference)){
                return timestamp;
            }

            if (days > 1) {
                return days + " days ago";
            } else if (days == 1) {
                return "yesterday"
            } else if (hours > 1) {
                return hours + " hours ago";
            } else if (hours == 1) {
                return "an hour ago";
            } else if (minutes > 1) {
                return minutes + " minutes ago";
            } else if (minutes == 1){
                return "a minute ago";
            } else {
                return "a few seconds ago";
            }
        }});
